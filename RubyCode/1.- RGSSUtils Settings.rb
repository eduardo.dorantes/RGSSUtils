#===============================================================================
# **  Utilities for RGSSx
#===============================================================================
module RGSSUtils
  
  # DLL Filename
  DLL_NAME        = "RGSSUtils"
  
  # Game Name (used as ID in the WR)
  GAME_NAME       = "Pokemon Essentials"
  
  # Web Request Method, can be GET or POST
  REQUEST_METHOD  = "POST"
  
  # User Agent used to perform the request
  USER_AGENT      = "PokemonEssentials WindowsClient"
  
  # KEY for Symmetric Data Encryption
  CIPHER_KEY      = "a439fcd8b52d43c7e731f6488f2bcaea"
end

# Initializes the library at top level
Win32API.new(RGSSUtils::DLL_NAME, "LoadLibrary", "", "").call()