module RGSSUtils
  module DownloadManager
    @@downloads = []
    
    def self.downloads
      @@downloads
    end
    
    def self.new(url, filename)
      download = FileDownload.new(url, filename)
      @@downloads.push(download)
      index = @@downloads.index(download)
      @@downloads[index].id = index
      @@downloads[index].start()
    end
    
    def self.update(id, progress)
      @@downloads[id].update(progress)
    end
  end
    
  class FileDownload
    attr_reader :url, :filename, :id, :progress
    attr_writer :id, :progress
    
    def initialize(url, filename)
      @url = url
      @filename = filename
      @id = 0
      @progress = 0
    end
    
    def start()
      RGSSUtils.download(@url, @filename, @id)
    end
    
    def update(progress)
      self.progress = progress
      puts "Download progress: #{self.progress}%"
    end
    
    def finish()
      
    end
  end
end