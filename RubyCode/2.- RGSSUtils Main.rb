module RGSSUtils
  DLL_ENCRYPT     = Win32API.new(DLL_NAME, 'Encrypt', 'PP', 'P')
  DLL_DECRYPT     = Win32API.new(DLL_NAME, 'Decrypt', 'PP', 'P')
  DLL_WEBREQUEST  = Win32API.new(DLL_NAME, 'WebRequest', 'PP', 'P')
  DLL_FILEDOWNLOAD= Win32API.new(DLL_NAME, 'FileDownload', 'PPI', 'I')
  DLL_CIPHER_SHA1 = Win32API.new(DLL_NAME, 'SHA1', 'P', 'P')
  DLL_CIPHER_MD5  = Win32API.new(DLL_NAME, 'MD5', 'P', 'P')
  DLL_SET_VALUE   = Win32API.new(DLL_NAME, 'SetValue', 'PP', 'V')
  DLL_GET_VALUE   = Win32API.new(DLL_NAME, 'GetValue', 'P', 'P')
  DLL_DUMP_DATA   = Win32API.new(DLL_NAME, 'DumpData', 'PP', 'V')
  DLL_LOAD_DATA   = Win32API.new(DLL_NAME, 'LoadData', 'PP', 'I')
  DLL_VERSION     = Win32API.new(DLL_NAME, 'DLLVersion', '', 'P')
  DLL_GET_MAC     = Win32API.new(DLL_NAME, 'GetMACAddress', '', 'P')
  DLL_EVAL        = Win32API.new(DLL_NAME, 'Eval', 'P', 'V')
  DLL_SCRIPTWIND  = Win32API.new(DLL_NAME, 'ShowScriptWindow', '', 'V')
  DLL_INITIALIZE  = Win32API.new(DLL_NAME, 'Initialize', 'PPPP', 'I')
  DLL_RGSSVersion = Win32API.new(DLL_NAME, 'RGSSVersion', '', 'I')

  @input = true

  # Initializes the DLL with Main Variables
  def self.init
    return DLL_INITIALIZE.call(GAME_NAME, CIPHER_KEY, REQUEST_METHOD, USER_AGENT) == 1
  end

  # Evaluates a chunk of code
  def self.eval(string)
    DLL_EVAL.call(string)
  end

  # Shows a Scripting Window
  def self.show_script_window()
    DLL_SCRIPTWIND.call()
  end

  # Disables Input
  def self.disable_input
    @input = false
  end

  # Enables Input
  def self.enable_input
    @input = true
  end

  # Gets Input Status
  def self.input?
    @input
  end

  # Gets the DLL Version
  def self.dll_version
    return DLL_VERSION.call()
  end

  # Gets the RGSS version
  def self.rgss_version
    case DLL_RGSSVersion.call()
    when 0
        return 'Undefined'
    when 1
        return 'RGSS1'
    when 2
        return 'RGSS2'
    when 3
        return 'RGSS3'
    else
        return 'Undefined'
    end
  end

  # Symmetric-encrypts the given string
  def self.encrypt(string)
    return DLL_ENCRYPT.call(string)
  end

  # Decrypts (or at least tries) the data
  def self.decrypt(string)
    return DLL_DECRYPT.call(string)
  end

  # Sends a request to the specified URL (as well as the post_data)
  def self.request(url, post_data = '')
    return DLL_WEBREQUEST.call(url, post_data)
  end
  
  # Downloads a file
  def self.download(url, file, id)
    return DLL_FILEDOWNLOAD.call(url, file, id)
  end

  # Generates a MD5 checksum from the passed string
  def self.md5(string)
    return DLL_CIPHER_MD5.call(string)
  end

  # Generates a SHA1 checksum of the given string
  def self.sha1(string)
    return DLL_CIPHER_SHA1.call(string)
  end

  # Stores data in an internal dictionary with a key (used to safely save data)
  def self.store(key, value)
    return DLL_SET_VALUE.call(key, value)
  end

  # Gets the data assigned to the specified key
  def self.get(key)
    return DLL_GET_VALUE.call(key)
  end

  # Writes into a file all the data in the internal dictionary
  def self.dump(filename)
    return DLL_DUMP_DATA.call(File.basename(filename), filename)
  end

  # Loads from the file the data into the internal dictionary
  def self.load(filename)
    return DLL_LOAD_DATA.call(File.basename(filename), filename)
  end

  # Returns the Default Network Device MAC Address
  def self.getMACAddress
    return DLL_GET_MAC.call()
  end

  # Serializes a Ruby Object
  def self.serialize(str)
    ret=[Zlib::Deflate.deflate(Marshal.dump(str))].pack('m')
    return ret
  end

  # Restores a Ruby Object from the String
  def self.restore(str)
    return '' if str==''
    ret=Marshal.restore(Zlib::Inflate.inflate(str.unpack('m')[0]))
    return ret
  end
end

RGSSUtils.init