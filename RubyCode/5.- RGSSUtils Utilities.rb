module Input
  class << self
    alias _update_utils update
    def update
      return if !RGSSUtils.input?
      _update_utils
      if $DEBUG && Input.trigger?(Input::ALT)
        RGSSUtils.show_script_window
      end
    end
  end
end