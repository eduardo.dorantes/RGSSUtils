module RGSSUtils
  class AsyncRequest
    attr_reader :filename, :url
    def initialize(filename, url, query)
      @filename = filename
      @url      = url
      @query    = query
    end

    def start
      @thread = Thread.new(@filename, @url, @query){|filename, url, query|
        result = RGSSUtils.request(url, query)
        File.open(filename, 'wb'){|f|
          f.write result
        }
      }
    end

    def status
      begin
        File.read(@filename) != ''
      rescue
        false
      end
    end

    def content
      data = ''
      File.open(@filename, 'rb'){ |f|
        data = f.read
      }
      return data
    end

    def delete
      @thread.join
      File.delete(@filename) rescue true
    end
  end

  def self.async_request(filename, url, query, command)
    filename = RGSSUtils.md5(url+query) if filename == :auto
    Thread.new(filename, url, query, command){|f, u, q, c|
      request = RGSSUtils::AsyncRequest.new(f,u,q)
      request.start
      while !request.status
        c.call(false)
      end
        c.call(true, request.content)
        request.delete
    }.join
  end
end