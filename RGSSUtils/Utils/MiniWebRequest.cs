﻿using System;
using System.Text;
using System.Net;
using System.IO;

namespace RGSSUtils
{
    public class MiniWebRequest
    {
        private HttpWebRequest request;
        public String UserAgent;
        private Stream dataStream;
        private string status;
        public String Status
        {
            get
            {
                return status;
            }
            set
            {
                status = value;
            }
        }

        public MiniWebRequest(string url, string user_agent)
        {
            request = (HttpWebRequest)WebRequest.Create(url);
            request.UserAgent = user_agent;
        }

        public MiniWebRequest(string url, string method, string user_agent)
            : this(url, user_agent)
        {

            if (method.Equals("GET") || method.Equals("POST"))
            {
                request.Method = method;
            }
            else
            {
                throw new Exception("Invalid Method Type");
            }
        }

        public MiniWebRequest(string url, string method, string data, string user_agent)
        : this(url, method, user_agent)
        {
            if (request.Method == "POST")
            {
                byte[] byteArray = Encoding.UTF8.GetBytes(data);
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = byteArray.Length;
                dataStream = request.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();
            }
            else
            {
                String finalUrl = string.Format("{0}{1}", url, "?" + data);
                request = (HttpWebRequest)WebRequest.Create(finalUrl);
                request.UserAgent = user_agent;
                WebResponse response = request.GetResponse();
                dataStream = response.GetResponseStream();
            }
        }

        public string GetResponse()
        {
            string responseFromServer = "";
            try
            {
                WebResponse response = request.GetResponse();
                this.Status = ((HttpWebResponse)response).StatusDescription;
                dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                responseFromServer = reader.ReadToEnd();
                reader.Close();
                dataStream.Close();
                response.Close();
            }
            catch (WebException ex)
            {
                string status = ex.Status.ToString();
                if (ex.Response != null)
                {
                    if (ex.Response.ContentLength != 0)
                    {
                        using (var stream = ex.Response.GetResponseStream())
                        {
                            using (var reader = new StreamReader(stream))
                            {
                                responseFromServer = reader.ReadToEnd();
                            }
                        }
                    }
                }
            }
            return responseFromServer;
        }

    }
}
