﻿using System.Linq;
using System.Net.NetworkInformation;

namespace RGSSUtils
{
    public static class GetComputerInfo
    {
        /// <summary>
        /// Gets the MAC Address
        /// </summary>
        /// <returns></returns>
        public static string getMACAddress()
        {
            var macAddr =
                (
                    from nic in NetworkInterface.GetAllNetworkInterfaces()
                    where nic.OperationalStatus == OperationalStatus.Up
                    select nic.GetPhysicalAddress().ToString()
                ).FirstOrDefault();

            return macAddr;
        }
    }
}
