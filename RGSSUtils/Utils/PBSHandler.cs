﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace RGSSUtils.Utils
{
    public static class PBSHandler
    {
        private static AutoCompleteStringCollection GetAllFromPokemon(string file, string pattern, string repl)
        {
            var collection = new AutoCompleteStringCollection();
            var data = File.ReadAllLines(file);
            var regex = new Regex(pattern);
            foreach (string item in data)
            {
                Match match = regex.Match(item);
                if (match.Success)
                {
                    string i = match.Value.Replace(repl, "");
                    collection.Add(i);
                }
            }
            return collection;
        }

        public static AutoCompleteStringCollection GetPokemonConstants()
        {
            return GetAllFromPokemon("PBS/pokemon.txt", @"^InternalName\=\w+$", "InternalName=");
        }

        public static AutoCompleteStringCollection GetAllMoves()
        {
            if (!File.Exists("PBS/moves.txt"))
            {
                return new AutoCompleteStringCollection();
            }

            var collection = new AutoCompleteStringCollection();
            var MoveData = File.ReadAllLines("PBS/moves.txt");
            var regex = new Regex(@"^\d+\,(\w+)\,");
            foreach (string line in MoveData)
            {
                Match match = regex.Match(line);
                string move = match.Groups[1].ToString();
                collection.Add(move);
            }
            return collection;
        }

        public static AutoCompleteStringCollection GetAllItems()
        {
            if (!File.Exists("PBS/items.txt"))
            {
                return new AutoCompleteStringCollection();
            }

            var collection = new AutoCompleteStringCollection();
            var ItemData = File.ReadAllLines("PBS/items.txt");
            var regex = new Regex(@"^\d+\,(\w+)\,\w+\,\w+\,(\d)");
            foreach(string line in ItemData)
            {
                Match match = regex.Match(line);
                if (match.Success)
                {
                    string type = match.Groups[2].ToString();
                    if (!String.IsNullOrWhiteSpace(type) && type != "4" && type != "8")
                    {
                        collection.Add(match.Groups[1].ToString());
                    }
                }
            }
            return collection;
        }

        public static AutoCompleteStringCollection GetAllTrainers()
        {
            if (!File.Exists("PBS/trainers.txt"))
            {
                return new AutoCompleteStringCollection();
            }
            var collection = new AutoCompleteStringCollection();
            var data = File.ReadAllLines("PBS/trainers.txt");
            var tmp = String.Join("|",data);
            var TrainerData = tmp.Split('#');
            var regex = new Regex(@"^\-*\|(\w+)\|(\w+(\W[&]\W)?\w+?)(\,(\d+))?\|");
            foreach(string trainer in TrainerData)
            {
                Match match = regex.Match(trainer);
                if (match.Success)
                {
                    string Trainer;
                    if (!String.IsNullOrWhiteSpace(match.Groups[4].ToString()))
                    {
                        Trainer = String.Format("{0} {1} ({2})", match.Groups[1], match.Groups[2], match.Groups[5]);
                    }
                    else
                    {
                        Trainer = String.Format("{0} {1}", match.Groups[1], match.Groups[2]);
                    }
                    collection.Add(Trainer);
                }
            }
            return collection;
        }

        public static AutoCompleteStringCollection GetAllCharacterTypes()
        {
            if (!File.Exists("PBS/metadata.txt"))
            {
                return new AutoCompleteStringCollection();
            }
            var collection = new AutoCompleteStringCollection();
            var fileData = File.ReadAllLines("PBS/metadata.txt");
            var regex = new Regex(@"^Player(\w)=(\w+)\,");
            foreach(string line in fileData)
            {
                Match match = regex.Match(line);
                if (match.Success)
                {
                    collection.Add(match.Groups[2].ToString());
                }
            }
            return collection;
        }
    }
}
