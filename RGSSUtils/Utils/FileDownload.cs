﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RGSSUtils.Utils
{
    public class FileDownload
    {
        private string url;
        private string filename;
        private int id;

        public FileDownload(string url, string filename, int id)
        {
            this.url = url;
            this.filename = filename;
            this.id = id;
        }

        public void Start()
        {
            using (WebClient wc = new WebClient())
            {
                wc.DownloadProgressChanged += wc_DownloadProgressChanged;
                wc.DownloadFileAsync(new System.Uri(this.url),this.filename);
            }
        }

        public void wc_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            RGSSx.Eval("RGSSUtils::DownloadManager.update(" +this.id + "," + e.ProgressPercentage + ");");
        }
    }
}
