﻿using Microsoft.Win32;

namespace RGSSUtils
{
    public static class WindowsRegistry
    {
        public static void Store(string _key, string value)
        {
            RegistryKey key = Registry.CurrentUser.CreateSubKey(string.Format("SOFTWARE\\{0}", Main.GameName));
            key.SetValue(_key, value);
            key.Close();
        }

        public static string Get(string _key)
        {
            RegistryKey key = Registry.CurrentUser.OpenSubKey(string.Format("SOFTWARE\\{0}", Main.GameName));
            string value = "";
            if (key != null)
            {
                value = key.GetValue(_key).ToString();
                key.Close();
            }
            return value;
        }
    }
}
