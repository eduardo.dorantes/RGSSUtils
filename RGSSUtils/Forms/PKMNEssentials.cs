﻿using System;
using System.IO;
using System.Windows.Forms;
using RGSSUtils.Forms.EssentialsCommands;
using RGSSUtils.Utils;

namespace RGSSUtils.Forms
{
    public partial class PKMNEssentials : Form
    {
        private string ErrorMessage = "Oh no! I can't evaluate that script!";
        public PKMNEssentials()
        {
            InitializeComponent();
        }

        private void PKMNEssentials_Load(object sender, EventArgs e)
        {
            if (File.Exists("PBS/pokemon.txt"))
            {
                pbWildBattle.Enabled = true;
                pbAddPokemon.Enabled = true;
            }

            if (File.Exists("PBS/trainers.txt"))
            {
                pbTrainerBattle.Enabled = true;
            }

            var types = PBSHandler.GetAllCharacterTypes();
            foreach(string type in types)
            {
                TrainerType.Items.Add(type);
            }
        }

        private void pbWildBattle_Click(object sender, EventArgs e)
        {
            WildBattle WildBattleWindow = new WildBattle();
            WildBattleWindow.Show();
            this.Hide();
        }

        private void pbGiveDemoParty_Click(object sender, EventArgs e)
        {
            Run("Kernel.pbCreatePokemon", "You have received the Demo Party.");
        }

        private void pbAddPokemon_Click(object sender, EventArgs e)
        {
            AddPokemon AddPokemonWindow = new AddPokemon();
            AddPokemonWindow.Show();
            this.Hide();
        }

        private void pbTrainerBattle_Click(object sender, EventArgs e)
        {
            TrainerBattle TrainerBattleWindow = new TrainerBattle();
            TrainerBattleWindow.Show();
            this.Hide();
        }

        private void TrainerType_SelectedIndexChanged(object sender, EventArgs e)
        {
            int type = TrainerType.SelectedIndex;
            string code = String.Format("pbChangePlayer({0});", type);
            Run(code, String.Format("Player's class now is {0}.", TrainerType.SelectedItem));
        }

        private void pbTrainerName_Click(object sender, EventArgs e)
        {
            string name = TrainerName.Text.Trim(' ');
            string code = String.Format("$Trainer.name = \"{0}\";", name);
            Run(code, String.Format("Trainer's name has been set to {0}.", name));
        }

        private void Input_CheckedChanged(object sender, EventArgs e)
        {
            if (Input.Checked)
            {
                enable_input();
            }
            else
            {
                RGSSx.Eval("RGSSUtils.disable_input;");
            }
        }

        private void SetMoney_Click(object sender, EventArgs e)
        {
            decimal money = MoneyInput.Value;
            string code = String.Format("$Trainer.money = {0};", money);
            Run(code, String.Format("Money has been set to {0}.", money));
        }

        private void enable_input()
        {
            RGSSx.Eval("RGSSUtils.enable_input;");
        }

        private void pbEmptyBag_Click(object sender, EventArgs e)
        {
            Run("$PokemonBag.clear;", "The Bag was cleared.");
        }

        private void pbFillBag_Click(object sender, EventArgs e)
        {
            string code = @"
            qty = 99
            itemconsts =[]
            for i in PBItems.constants
                itemconsts.push(PBItems.const_get(i))
            end
            itemconsts.sort!{| a,b | a <=> b}
            for i in itemconsts
                $PokemonBag.pbStoreItem(i, qty)    
            end";
            Run(code, "Bag has been filled with 99 of each item.");
        }

        private void pbRunningShoes_Click(object sender, EventArgs e)
        {
            Run("$PokemonGlobal.runningShoes=!$PokemonGlobal.runningShoes;", "You have toggled Running Shoes!");
        }

        private void pbPokeGear_Click(object sender, EventArgs e)
        {
            Run("$Trainer.pokegear=!$Trainer.pokegear;", "You have toggled the PokeGear!");
        }

        private void pbPokedex_Click(object sender, EventArgs e)
        {
            Run("$Trainer.pokedex=!$Trainer.pokedex;", "You have toggled the Pokédex!");
        }

        private void pbFillStorage_Click(object sender, EventArgs e)
        {
            string code = @"
              $Trainer.formseen=[] if !$Trainer.formseen
              $Trainer.formlastseen=[] if !$Trainer.formlastseen
              added=0; completed=true
              for i in 1..PBSpecies.maxValue
                if added>=STORAGEBOXES*30
                  completed=false; break
                end
                cname=getConstantName(PBSpecies,i) rescue nil
                next if !cname
                pkmn=PokeBattle_Pokemon.new(i,50,$Trainer)
                $PokemonStorage[(i-1)/$PokemonStorage.maxPokemon(0),
                                (i-1)%$PokemonStorage.maxPokemon(0)]=pkmn
                $Trainer.seen[i]=true
                $Trainer.owned[i]=true
                $Trainer.formlastseen[i]=[] if !$Trainer.formlastseen[i]
                $Trainer.formlastseen[i]=[0,0] if $Trainer.formlastseen[i]==[]
                $Trainer.formseen[i]=[[],[]] if !$Trainer.formseen[i]
                for j in 0..27
                  $Trainer.formseen[i][0][j]=true
                  $Trainer.formseen[i][1][j]=true
                end
                added+=1
              end";
            Run(code, "Boxes were filled with one Pokémon of each species.");
        }

        private void pbClearStorage_Click(object sender, EventArgs e)
        {
            string code = @"
              for i in 0...$PokemonStorage.maxBoxes
                for j in 0...$PokemonStorage.maxPokemon(i)
                  $PokemonStorage[i,j]=nil
                end
              end";
            Run(code, "The Boxes were cleared.");
        }

        private void pbHealParty_Click(object sender, EventArgs e)
        {
            string code = @"
              for i in $Trainer.party
                i.heal
              end";
            Run(code, "Your Pokémon were healed.");
        }

        private void pbQuickHatch_Click(object sender, EventArgs e)
        {
            string code = @"
              for pokemon in $Trainer.party
                pokemon.eggsteps=1 if pokemon.isEgg?
              end";
            Run(code, "All eggs on your party now require one step to hatch.");
        }

        private void Run(string code, string message)
        {
            if (RGSSx.Eval(code))
            {
                StatusLabel.Text = message;
            }
            else
            {
                StatusLabel.Text = ErrorMessage;
            }
        }

        private void pbUsePC_Click(object sender, EventArgs e)
        {
            Run("pbPokeCenterPC();", "PC Started.");
        }
    }
}
