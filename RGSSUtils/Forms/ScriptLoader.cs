﻿using System;
using System.IO;
using System.Windows.Forms;

namespace RGSSUtils.Forms
{
    public partial class ScriptLoader : Form
    {
        public ScriptLoader()
        {
            InitializeComponent();
        }

        private void EvalButton_Click(object sender, EventArgs e)
        {
            this.Enabled = false;
            if (RGSSx.Eval(ScriptBox.Text))
            {
                ScriptStatus.Text = "Success!";
            }
            else
            {
                ScriptStatus.Text = "Failure!";
            }
            this.Enabled = true;
        }

        private void ScriptLoader_Load(object sender, EventArgs e)
        {
            if (Directory.Exists("PBS")){
                PKMNEssentials.Visible = true;
            }
            else
            {
                PKMNEssentials.Visible = false;
            }
        }

        private void InputEnabled_CheckedChanged(object sender, EventArgs e)
        {
            if (InputEnabled.Checked)
            {
                RGSSx.Eval("RGSSUtils.enable_input");
            }
            else
            {
                RGSSx.Eval("RGSSUtils.disable_input");
            }
        }

        private void PKMNEssentials_Click(object sender, EventArgs e)
        {
            PKMNEssentials EssentialsCommands = new PKMNEssentials();
            EssentialsCommands.Show();
            RGSSx.Eval("RGSSUtils.enable_input");
            Main.ScriptWindowLoaded = false;
            this.Hide();
        }

        private void ClearScript_Click(object sender, EventArgs e)
        {
            ScriptBox.Text = "";
        }
    }
}
