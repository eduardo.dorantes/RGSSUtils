﻿namespace RGSSUtils.Forms.EssentialsCommands
{
    partial class WildBattle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.PKMN1 = new System.Windows.Forms.TextBox();
            this.PKMN2 = new System.Windows.Forms.TextBox();
            this.LEVEL1 = new System.Windows.Forms.NumericUpDown();
            this.LEVEL2 = new System.Windows.Forms.NumericUpDown();
            this.UseDoubleBattle = new System.Windows.Forms.CheckBox();
            this.StartBattle = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.LEVEL1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LEVEL2)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Pokémon:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(135, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Level:";
            // 
            // PKMN1
            // 
            this.PKMN1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.PKMN1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.PKMN1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.PKMN1.Location = new System.Drawing.Point(23, 39);
            this.PKMN1.MaxLength = 25;
            this.PKMN1.Name = "PKMN1";
            this.PKMN1.Size = new System.Drawing.Size(100, 20);
            this.PKMN1.TabIndex = 2;
            // 
            // PKMN2
            // 
            this.PKMN2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.PKMN2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.PKMN2.Enabled = false;
            this.PKMN2.Location = new System.Drawing.Point(23, 65);
            this.PKMN2.MaxLength = 25;
            this.PKMN2.Name = "PKMN2";
            this.PKMN2.Size = new System.Drawing.Size(100, 20);
            this.PKMN2.TabIndex = 3;
            // 
            // LEVEL1
            // 
            this.LEVEL1.Location = new System.Drawing.Point(138, 39);
            this.LEVEL1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.LEVEL1.Name = "LEVEL1";
            this.LEVEL1.Size = new System.Drawing.Size(76, 20);
            this.LEVEL1.TabIndex = 4;
            this.LEVEL1.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // LEVEL2
            // 
            this.LEVEL2.Enabled = false;
            this.LEVEL2.Location = new System.Drawing.Point(138, 65);
            this.LEVEL2.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.LEVEL2.Name = "LEVEL2";
            this.LEVEL2.Size = new System.Drawing.Size(76, 20);
            this.LEVEL2.TabIndex = 5;
            this.LEVEL2.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // UseDoubleBattle
            // 
            this.UseDoubleBattle.AutoSize = true;
            this.UseDoubleBattle.Location = new System.Drawing.Point(236, 67);
            this.UseDoubleBattle.Name = "UseDoubleBattle";
            this.UseDoubleBattle.Size = new System.Drawing.Size(90, 17);
            this.UseDoubleBattle.TabIndex = 6;
            this.UseDoubleBattle.Text = "Double Battle";
            this.UseDoubleBattle.UseVisualStyleBackColor = true;
            this.UseDoubleBattle.CheckedChanged += new System.EventHandler(this.UseDoubleBattle_CheckedChanged);
            // 
            // StartBattle
            // 
            this.StartBattle.Location = new System.Drawing.Point(236, 39);
            this.StartBattle.Name = "StartBattle";
            this.StartBattle.Size = new System.Drawing.Size(75, 23);
            this.StartBattle.TabIndex = 7;
            this.StartBattle.Text = "Run";
            this.StartBattle.UseVisualStyleBackColor = true;
            this.StartBattle.Click += new System.EventHandler(this.StartBattle_Click);
            // 
            // WildBattle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(340, 94);
            this.Controls.Add(this.StartBattle);
            this.Controls.Add(this.UseDoubleBattle);
            this.Controls.Add(this.LEVEL2);
            this.Controls.Add(this.LEVEL1);
            this.Controls.Add(this.PKMN2);
            this.Controls.Add(this.PKMN1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "WildBattle";
            this.ShowIcon = false;
            this.Text = "Wild Battle";
            this.Load += new System.EventHandler(this.WildBattle_Load);
            ((System.ComponentModel.ISupportInitialize)(this.LEVEL1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LEVEL2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox PKMN1;
        private System.Windows.Forms.TextBox PKMN2;
        private System.Windows.Forms.NumericUpDown LEVEL1;
        private System.Windows.Forms.NumericUpDown LEVEL2;
        private System.Windows.Forms.CheckBox UseDoubleBattle;
        private System.Windows.Forms.Button StartBattle;
    }
}