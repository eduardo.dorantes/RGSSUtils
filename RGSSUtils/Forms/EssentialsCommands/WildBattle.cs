﻿using System;
using System.Windows.Forms;
using RGSSUtils.Utils;

namespace RGSSUtils.Forms.EssentialsCommands
{
    public partial class WildBattle : Form
    {
        public WildBattle()
        {
            InitializeComponent();
            var PKMNCollection = PBSHandler.GetPokemonConstants();
            PKMN1.AutoCompleteCustomSource = PKMNCollection;
            PKMN2.AutoCompleteCustomSource = PKMNCollection;
        }

        private void StartBattle_Click(object sender, EventArgs e)
        {
            if (UseDoubleBattle.Checked)
            {
                string pkmn1 = PKMN1.Text;
                string pkmn2 = PKMN2.Text;
                if (!String.IsNullOrWhiteSpace(pkmn1) && !String.IsNullOrWhiteSpace(pkmn2))
                {
                    string code = String.Format("Kernel.pbDoubleWildBattle(:{0}, {1}, :{2}, {3})",
                                                pkmn1.ToUpper().Trim(' '), LEVEL1.Value,
                                                pkmn2.ToUpper().Trim(' '), LEVEL2.Value);
                    run(code);
                }
            }
            else
            {
                if (!String.IsNullOrWhiteSpace(PKMN1.Text))
                {
                    string code = String.Format("Kernel.pbWildBattle(:{0}, {1})",
                                                PKMN1.Text.ToUpper().Trim(' '), LEVEL1.Value);
                    run(code);
                }
            }
        }

        private void run(string code)
        {
            this.Enabled = false;
            RGSSx.Eval(code);
            this.Hide();
        }

        private void UseDoubleBattle_CheckedChanged(object sender, EventArgs e)
        {
            PKMN2.Enabled = UseDoubleBattle.Checked;
            LEVEL2.Enabled = UseDoubleBattle.Checked;
        }

        private void WildBattle_Load(object sender, EventArgs e)
        {

        }
    }
}
