﻿using System;
using System.IO;
using System.Windows.Forms;
using RGSSUtils.Utils;
using System.Linq;

namespace RGSSUtils.Forms.EssentialsCommands
{
    public partial class AddPokemon : Form
    {
        public AddPokemon()
        {
            InitializeComponent();
            if (File.Exists("PBS/items.txt"))
            {
                ItemSelector.Enabled = true;
            }
            SpeciesSelector.AutoCompleteCustomSource = PBSHandler.GetPokemonConstants();

            var Moves = PBSHandler.GetAllMoves();
            Move1.AutoCompleteCustomSource = Moves;
            Move2.AutoCompleteCustomSource = Moves;
            Move3.AutoCompleteCustomSource = Moves;
            Move4.AutoCompleteCustomSource = Moves;

            ItemSelector.AutoCompleteCustomSource = PBSHandler.GetAllItems();
        }

        private void AdvancedSettingsToggler_CheckedChanged(object sender, EventArgs e)
        {
            Moves.Enabled = AdvancedSettingsToggler.Checked;
            OtherSettings.Enabled = AdvancedSettingsToggler.Checked;
        }

        private void CreatePokemon_Click(object sender, EventArgs e)
        {
            this.Enabled = false;
            string code = "";
            if (String.IsNullOrWhiteSpace(SpeciesSelector.Text))
            {
                return;
            }
            string species = SpeciesSelector.Text;
            string level = LevelSelector.Value.ToString();
            string trainer;
            if (OwnedByTrainer.Checked)
            {
                trainer = "$Trainer";
            }
            else
            {
                code += "trainer = PokeBattle_Trainer.new('Trainer', 0);";
                trainer = "trainer";
            }
            code += String.Format("pkmn = PokeBattle_Pokemon.new(:{0}, {1}, {2});", species, level, trainer);
            if (GenderFlag.Checked)
            {
                code += "pkmn.makeFemale();";
            }
            if (ShinyFlag.Checked)
            {
                code += "pkmn.makeShiny();";
            }
            if (isEgg.Checked)
            {
                code += "pkmn.eggsteps = 255;";
            }
            if (AdvancedSettingsToggler.Checked)
            {
                string[] moves = new String[4] { Move1.Text, Move2.Text, Move3.Text, Move4.Text };
                bool cleaned = false;
                moves = moves.Distinct().ToArray();
                foreach (string move in moves)
                {
                    string mov = move.ToUpper().Trim(' ');
                    if (String.IsNullOrWhiteSpace(mov))
                    {
                        continue;
                    }
                    if (!cleaned)
                    {
                        code += "pkmn.pbDeleteAllMoves();";
                        cleaned = true;
                    }
                    code += String.Format("pkmn.pbLearnMove(:{0});", mov);
                }
                code += String.Format("pkmn.happiness = {0};", Happiness.Value);
                string item = ItemSelector.Text.Trim(' ').ToUpper();
                if (!String.IsNullOrWhiteSpace(item))
                {
                    code += String.Format("pkmn.setItem(:{0});", item);
                }
            }
            code += "pbNicknameAndStore(pkmn);";
            RGSSx.Eval(code);
            this.Hide();
        }

        private void AddPokemon_Load(object sender, EventArgs e)
        {

        }
    }
}
