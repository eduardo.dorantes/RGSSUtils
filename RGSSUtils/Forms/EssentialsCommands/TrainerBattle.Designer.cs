﻿namespace RGSSUtils.Forms.EssentialsCommands
{
    partial class TrainerBattle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.TrainerSelection = new System.Windows.Forms.ListBox();
            this.DoubleTrainer = new System.Windows.Forms.CheckBox();
            this.DoubleBattle = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Trainer2Selection = new System.Windows.Forms.ListBox();
            this.StartBattle = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Trainer:";
            // 
            // TrainerSelection
            // 
            this.TrainerSelection.FormattingEnabled = true;
            this.TrainerSelection.Location = new System.Drawing.Point(15, 25);
            this.TrainerSelection.Name = "TrainerSelection";
            this.TrainerSelection.Size = new System.Drawing.Size(140, 173);
            this.TrainerSelection.TabIndex = 1;
            // 
            // DoubleTrainer
            // 
            this.DoubleTrainer.AutoSize = true;
            this.DoubleTrainer.Location = new System.Drawing.Point(172, 204);
            this.DoubleTrainer.Name = "DoubleTrainer";
            this.DoubleTrainer.Size = new System.Drawing.Size(96, 17);
            this.DoubleTrainer.TabIndex = 2;
            this.DoubleTrainer.Text = "Double Trainer";
            this.DoubleTrainer.UseVisualStyleBackColor = true;
            this.DoubleTrainer.CheckedChanged += new System.EventHandler(this.DoubleTrainer_CheckedChanged);
            // 
            // DoubleBattle
            // 
            this.DoubleBattle.AutoSize = true;
            this.DoubleBattle.Location = new System.Drawing.Point(15, 204);
            this.DoubleBattle.Name = "DoubleBattle";
            this.DoubleBattle.Size = new System.Drawing.Size(90, 17);
            this.DoubleBattle.TabIndex = 3;
            this.DoubleBattle.Text = "Double Battle";
            this.DoubleBattle.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(169, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Trainer 2:";
            // 
            // Trainer2Selection
            // 
            this.Trainer2Selection.Enabled = false;
            this.Trainer2Selection.FormattingEnabled = true;
            this.Trainer2Selection.Location = new System.Drawing.Point(171, 25);
            this.Trainer2Selection.Name = "Trainer2Selection";
            this.Trainer2Selection.Size = new System.Drawing.Size(140, 173);
            this.Trainer2Selection.TabIndex = 5;
            // 
            // StartBattle
            // 
            this.StartBattle.Location = new System.Drawing.Point(236, 227);
            this.StartBattle.Name = "StartBattle";
            this.StartBattle.Size = new System.Drawing.Size(75, 23);
            this.StartBattle.TabIndex = 6;
            this.StartBattle.Text = "Start Battle!";
            this.StartBattle.UseVisualStyleBackColor = true;
            this.StartBattle.Click += new System.EventHandler(this.StartBattle_Click);
            // 
            // TrainerBattle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(326, 261);
            this.Controls.Add(this.StartBattle);
            this.Controls.Add(this.Trainer2Selection);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.DoubleBattle);
            this.Controls.Add(this.DoubleTrainer);
            this.Controls.Add(this.TrainerSelection);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "TrainerBattle";
            this.Text = "Trainer Battle";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox TrainerSelection;
        private System.Windows.Forms.CheckBox DoubleTrainer;
        private System.Windows.Forms.CheckBox DoubleBattle;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox Trainer2Selection;
        private System.Windows.Forms.Button StartBattle;
    }
}