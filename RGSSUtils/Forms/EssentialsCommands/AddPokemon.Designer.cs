﻿namespace RGSSUtils.Forms.EssentialsCommands
{
    partial class AddPokemon
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.SpeciesSelector = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.LevelSelector = new System.Windows.Forms.NumericUpDown();
            this.isEgg = new System.Windows.Forms.CheckBox();
            this.BasicStuff = new System.Windows.Forms.GroupBox();
            this.OwnedByTrainer = new System.Windows.Forms.CheckBox();
            this.GenderFlag = new System.Windows.Forms.CheckBox();
            this.ShinyFlag = new System.Windows.Forms.CheckBox();
            this.Moves = new System.Windows.Forms.GroupBox();
            this.Move4 = new System.Windows.Forms.TextBox();
            this.Move2 = new System.Windows.Forms.TextBox();
            this.Move3 = new System.Windows.Forms.TextBox();
            this.Move1 = new System.Windows.Forms.TextBox();
            this.CreatePokemon = new System.Windows.Forms.Button();
            this.AdvancedSettingsToggler = new System.Windows.Forms.CheckBox();
            this.OtherSettings = new System.Windows.Forms.GroupBox();
            this.ItemSelector = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.Happiness = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.LevelSelector)).BeginInit();
            this.BasicStuff.SuspendLayout();
            this.Moves.SuspendLayout();
            this.OtherSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Happiness)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Species: ";
            // 
            // SpeciesSelector
            // 
            this.SpeciesSelector.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.SpeciesSelector.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.SpeciesSelector.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.SpeciesSelector.Location = new System.Drawing.Point(70, 19);
            this.SpeciesSelector.MaxLength = 25;
            this.SpeciesSelector.Name = "SpeciesSelector";
            this.SpeciesSelector.Size = new System.Drawing.Size(100, 20);
            this.SpeciesSelector.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Level: ";
            // 
            // LevelSelector
            // 
            this.LevelSelector.Location = new System.Drawing.Point(70, 47);
            this.LevelSelector.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.LevelSelector.Name = "LevelSelector";
            this.LevelSelector.Size = new System.Drawing.Size(100, 20);
            this.LevelSelector.TabIndex = 3;
            this.LevelSelector.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // isEgg
            // 
            this.isEgg.AutoSize = true;
            this.isEgg.Location = new System.Drawing.Point(198, 73);
            this.isEgg.Name = "isEgg";
            this.isEgg.Size = new System.Drawing.Size(62, 17);
            this.isEgg.TabIndex = 4;
            this.isEgg.Text = "Is Egg?";
            this.isEgg.UseVisualStyleBackColor = true;
            // 
            // BasicStuff
            // 
            this.BasicStuff.Controls.Add(this.OwnedByTrainer);
            this.BasicStuff.Controls.Add(this.GenderFlag);
            this.BasicStuff.Controls.Add(this.ShinyFlag);
            this.BasicStuff.Controls.Add(this.SpeciesSelector);
            this.BasicStuff.Controls.Add(this.isEgg);
            this.BasicStuff.Controls.Add(this.label1);
            this.BasicStuff.Controls.Add(this.LevelSelector);
            this.BasicStuff.Controls.Add(this.label2);
            this.BasicStuff.Location = new System.Drawing.Point(12, 12);
            this.BasicStuff.Name = "BasicStuff";
            this.BasicStuff.Size = new System.Drawing.Size(283, 101);
            this.BasicStuff.TabIndex = 5;
            this.BasicStuff.TabStop = false;
            this.BasicStuff.Text = "Basic Settings";
            // 
            // OwnedByTrainer
            // 
            this.OwnedByTrainer.AutoSize = true;
            this.OwnedByTrainer.Checked = true;
            this.OwnedByTrainer.CheckState = System.Windows.Forms.CheckState.Checked;
            this.OwnedByTrainer.Location = new System.Drawing.Point(70, 73);
            this.OwnedByTrainer.Name = "OwnedByTrainer";
            this.OwnedByTrainer.Size = new System.Drawing.Size(116, 17);
            this.OwnedByTrainer.TabIndex = 7;
            this.OwnedByTrainer.Text = "Owned by $Trainer";
            this.OwnedByTrainer.UseVisualStyleBackColor = true;
            // 
            // GenderFlag
            // 
            this.GenderFlag.AutoSize = true;
            this.GenderFlag.Location = new System.Drawing.Point(198, 19);
            this.GenderFlag.Name = "GenderFlag";
            this.GenderFlag.Size = new System.Drawing.Size(77, 17);
            this.GenderFlag.TabIndex = 6;
            this.GenderFlag.Text = "Is Female?";
            this.GenderFlag.UseVisualStyleBackColor = true;
            // 
            // ShinyFlag
            // 
            this.ShinyFlag.AutoSize = true;
            this.ShinyFlag.Location = new System.Drawing.Point(198, 47);
            this.ShinyFlag.Name = "ShinyFlag";
            this.ShinyFlag.Size = new System.Drawing.Size(69, 17);
            this.ShinyFlag.TabIndex = 5;
            this.ShinyFlag.Text = "Is Shiny?";
            this.ShinyFlag.UseVisualStyleBackColor = true;
            // 
            // Moves
            // 
            this.Moves.Controls.Add(this.Move4);
            this.Moves.Controls.Add(this.Move2);
            this.Moves.Controls.Add(this.Move3);
            this.Moves.Controls.Add(this.Move1);
            this.Moves.Enabled = false;
            this.Moves.Location = new System.Drawing.Point(12, 119);
            this.Moves.Name = "Moves";
            this.Moves.Size = new System.Drawing.Size(283, 80);
            this.Moves.TabIndex = 6;
            this.Moves.TabStop = false;
            this.Moves.Text = "Moves";
            // 
            // Move4
            // 
            this.Move4.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.Move4.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.Move4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Move4.Location = new System.Drawing.Point(145, 45);
            this.Move4.MaxLength = 20;
            this.Move4.Name = "Move4";
            this.Move4.Size = new System.Drawing.Size(100, 20);
            this.Move4.TabIndex = 3;
            // 
            // Move2
            // 
            this.Move2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.Move2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.Move2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Move2.Location = new System.Drawing.Point(145, 19);
            this.Move2.MaxLength = 20;
            this.Move2.Name = "Move2";
            this.Move2.Size = new System.Drawing.Size(100, 20);
            this.Move2.TabIndex = 2;
            // 
            // Move3
            // 
            this.Move3.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.Move3.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.Move3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Move3.Location = new System.Drawing.Point(36, 45);
            this.Move3.MaxLength = 20;
            this.Move3.Name = "Move3";
            this.Move3.Size = new System.Drawing.Size(100, 20);
            this.Move3.TabIndex = 1;
            // 
            // Move1
            // 
            this.Move1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.Move1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.Move1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Move1.Location = new System.Drawing.Point(36, 19);
            this.Move1.MaxLength = 20;
            this.Move1.Name = "Move1";
            this.Move1.Size = new System.Drawing.Size(100, 20);
            this.Move1.TabIndex = 0;
            // 
            // CreatePokemon
            // 
            this.CreatePokemon.Location = new System.Drawing.Point(187, 250);
            this.CreatePokemon.Name = "CreatePokemon";
            this.CreatePokemon.Size = new System.Drawing.Size(108, 23);
            this.CreatePokemon.TabIndex = 7;
            this.CreatePokemon.Text = "Add Pokémon";
            this.CreatePokemon.UseVisualStyleBackColor = true;
            this.CreatePokemon.Click += new System.EventHandler(this.CreatePokemon_Click);
            // 
            // AdvancedSettingsToggler
            // 
            this.AdvancedSettingsToggler.AutoSize = true;
            this.AdvancedSettingsToggler.Location = new System.Drawing.Point(12, 254);
            this.AdvancedSettingsToggler.Name = "AdvancedSettingsToggler";
            this.AdvancedSettingsToggler.Size = new System.Drawing.Size(116, 17);
            this.AdvancedSettingsToggler.TabIndex = 8;
            this.AdvancedSettingsToggler.Text = "Advanced Settings";
            this.AdvancedSettingsToggler.UseVisualStyleBackColor = true;
            this.AdvancedSettingsToggler.CheckedChanged += new System.EventHandler(this.AdvancedSettingsToggler_CheckedChanged);
            // 
            // OtherSettings
            // 
            this.OtherSettings.Controls.Add(this.ItemSelector);
            this.OtherSettings.Controls.Add(this.label4);
            this.OtherSettings.Controls.Add(this.Happiness);
            this.OtherSettings.Controls.Add(this.label3);
            this.OtherSettings.Enabled = false;
            this.OtherSettings.Location = new System.Drawing.Point(14, 205);
            this.OtherSettings.Name = "OtherSettings";
            this.OtherSettings.Size = new System.Drawing.Size(281, 43);
            this.OtherSettings.TabIndex = 9;
            this.OtherSettings.TabStop = false;
            this.OtherSettings.Text = "Other";
            // 
            // ItemSelector
            // 
            this.ItemSelector.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ItemSelector.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.ItemSelector.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.ItemSelector.Enabled = false;
            this.ItemSelector.Location = new System.Drawing.Point(173, 14);
            this.ItemSelector.MaxLength = 20;
            this.ItemSelector.Name = "ItemSelector";
            this.ItemSelector.Size = new System.Drawing.Size(92, 20);
            this.ItemSelector.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(140, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Item: ";
            // 
            // Happiness
            // 
            this.Happiness.Location = new System.Drawing.Point(72, 14);
            this.Happiness.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.Happiness.Name = "Happiness";
            this.Happiness.Size = new System.Drawing.Size(62, 20);
            this.Happiness.TabIndex = 1;
            this.Happiness.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Happiness:";
            // 
            // AddPokemon
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(308, 283);
            this.Controls.Add(this.OtherSettings);
            this.Controls.Add(this.AdvancedSettingsToggler);
            this.Controls.Add(this.CreatePokemon);
            this.Controls.Add(this.Moves);
            this.Controls.Add(this.BasicStuff);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "AddPokemon";
            this.ShowIcon = false;
            this.Text = "Add Pokémon";
            this.Load += new System.EventHandler(this.AddPokemon_Load);
            ((System.ComponentModel.ISupportInitialize)(this.LevelSelector)).EndInit();
            this.BasicStuff.ResumeLayout(false);
            this.BasicStuff.PerformLayout();
            this.Moves.ResumeLayout(false);
            this.Moves.PerformLayout();
            this.OtherSettings.ResumeLayout(false);
            this.OtherSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Happiness)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox SpeciesSelector;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown LevelSelector;
        private System.Windows.Forms.CheckBox isEgg;
        private System.Windows.Forms.GroupBox BasicStuff;
        private System.Windows.Forms.CheckBox OwnedByTrainer;
        private System.Windows.Forms.CheckBox GenderFlag;
        private System.Windows.Forms.CheckBox ShinyFlag;
        private System.Windows.Forms.GroupBox Moves;
        private System.Windows.Forms.Button CreatePokemon;
        private System.Windows.Forms.CheckBox AdvancedSettingsToggler;
        private System.Windows.Forms.TextBox Move4;
        private System.Windows.Forms.TextBox Move2;
        private System.Windows.Forms.TextBox Move3;
        private System.Windows.Forms.TextBox Move1;
        private System.Windows.Forms.GroupBox OtherSettings;
        private System.Windows.Forms.TextBox ItemSelector;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown Happiness;
        private System.Windows.Forms.Label label3;
    }
}