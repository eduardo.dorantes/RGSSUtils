﻿using RGSSUtils.Utils;
using System;
using System.Windows.Forms;

namespace RGSSUtils.Forms.EssentialsCommands
{
    public partial class TrainerBattle : Form
    {
        public TrainerBattle()
        {
            InitializeComponent();
            var TempTrainerList = PBSHandler.GetAllTrainers();
            foreach (string trainer in TempTrainerList)
            {
                TrainerSelection.Items.Add(trainer);
                Trainer2Selection.Items.Add(trainer);
            }
        }

        private void DoubleTrainer_CheckedChanged(object sender, EventArgs e)
        {
            Trainer2Selection.Enabled = DoubleTrainer.Checked;
            DoubleBattle.Enabled = !DoubleTrainer.Checked;
        }

        private void StartBattle_Click(object sender, EventArgs e)
        {
            this.Enabled = false;
            int trainer1 = TrainerSelection.SelectedIndex;
            string code = "";
            code += "trainers=load_data(\"Data/trainers.dat\");";
            code += String.Format("trainer1 = trainers[{0}];", trainer1);
            if (DoubleTrainer.Checked)
            {
                int trainer2 = Trainer2Selection.SelectedIndex;
                code += String.Format("trainer2 = trainers[{0}];", trainer2);
                code += "pbDoubleTrainerBattle(trainer1[0],trainer1[1],trainer1[4],'...',trainer2[0],trainer2[1],trainer2[4],'...',true);";
            }
            else if (DoubleBattle.Checked)
            {
                code += @"pbTrainerBattle(trainer1[0], trainer1[1],'...',true,trainer1[4],true);";

            }
            else
            {
                code += @"pbTrainerBattle(trainer1[0], trainer1[1],'...',false,trainer1[4],true);";
            }
            RGSSx.Eval(code);
            this.Hide();

        }
    }
}
