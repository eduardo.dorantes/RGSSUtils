﻿namespace RGSSUtils.Forms
{
    partial class PKMNEssentials
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pbWildBattle = new System.Windows.Forms.Button();
            this.pbTrainerBattle = new System.Windows.Forms.Button();
            this.pbGiveDemoParty = new System.Windows.Forms.Button();
            this.pbFillBag = new System.Windows.Forms.Button();
            this.pbAddPokemon = new System.Windows.Forms.Button();
            this.pbFillStorage = new System.Windows.Forms.Button();
            this.BattleCodes = new System.Windows.Forms.GroupBox();
            this.TrainerCodes = new System.Windows.Forms.GroupBox();
            this.SetMoney = new System.Windows.Forms.Button();
            this.MoneyInput = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.pbTrainerName = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.TrainerType = new System.Windows.Forms.ListBox();
            this.TrainerName = new System.Windows.Forms.TextBox();
            this.pbUsePC = new System.Windows.Forms.Button();
            this.pbPokedex = new System.Windows.Forms.Button();
            this.pbPokeGear = new System.Windows.Forms.Button();
            this.pbRunningShoes = new System.Windows.Forms.Button();
            this.pbEmptyBag = new System.Windows.Forms.Button();
            this.pbClearStorage = new System.Windows.Forms.Button();
            this.PokemonCodes = new System.Windows.Forms.GroupBox();
            this.pbQuickHatch = new System.Windows.Forms.Button();
            this.pbHealParty = new System.Windows.Forms.Button();
            this.Input = new System.Windows.Forms.CheckBox();
            this.StatusStrip = new System.Windows.Forms.StatusStrip();
            this.StatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.BattleCodes.SuspendLayout();
            this.TrainerCodes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MoneyInput)).BeginInit();
            this.PokemonCodes.SuspendLayout();
            this.StatusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // pbWildBattle
            // 
            this.pbWildBattle.Enabled = false;
            this.pbWildBattle.Location = new System.Drawing.Point(129, 19);
            this.pbWildBattle.Name = "pbWildBattle";
            this.pbWildBattle.Size = new System.Drawing.Size(100, 23);
            this.pbWildBattle.TabIndex = 1;
            this.pbWildBattle.Text = "Wild Battle";
            this.pbWildBattle.UseVisualStyleBackColor = true;
            this.pbWildBattle.Click += new System.EventHandler(this.pbWildBattle_Click);
            // 
            // pbTrainerBattle
            // 
            this.pbTrainerBattle.Enabled = false;
            this.pbTrainerBattle.Location = new System.Drawing.Point(23, 19);
            this.pbTrainerBattle.Name = "pbTrainerBattle";
            this.pbTrainerBattle.Size = new System.Drawing.Size(100, 23);
            this.pbTrainerBattle.TabIndex = 0;
            this.pbTrainerBattle.Text = "Trainer Battle";
            this.pbTrainerBattle.UseVisualStyleBackColor = true;
            this.pbTrainerBattle.Click += new System.EventHandler(this.pbTrainerBattle_Click);
            // 
            // pbGiveDemoParty
            // 
            this.pbGiveDemoParty.Location = new System.Drawing.Point(23, 20);
            this.pbGiveDemoParty.Name = "pbGiveDemoParty";
            this.pbGiveDemoParty.Size = new System.Drawing.Size(100, 23);
            this.pbGiveDemoParty.TabIndex = 14;
            this.pbGiveDemoParty.Text = "Give Demo Party";
            this.pbGiveDemoParty.UseVisualStyleBackColor = true;
            this.pbGiveDemoParty.Click += new System.EventHandler(this.pbGiveDemoParty_Click);
            // 
            // pbFillBag
            // 
            this.pbFillBag.Location = new System.Drawing.Point(23, 81);
            this.pbFillBag.Name = "pbFillBag";
            this.pbFillBag.Size = new System.Drawing.Size(100, 23);
            this.pbFillBag.TabIndex = 6;
            this.pbFillBag.Text = "Fill Bag";
            this.pbFillBag.UseVisualStyleBackColor = true;
            this.pbFillBag.Click += new System.EventHandler(this.pbFillBag_Click);
            // 
            // pbAddPokemon
            // 
            this.pbAddPokemon.Enabled = false;
            this.pbAddPokemon.Location = new System.Drawing.Point(129, 20);
            this.pbAddPokemon.Name = "pbAddPokemon";
            this.pbAddPokemon.Size = new System.Drawing.Size(100, 23);
            this.pbAddPokemon.TabIndex = 15;
            this.pbAddPokemon.Text = "Add Pokémon";
            this.pbAddPokemon.UseVisualStyleBackColor = true;
            this.pbAddPokemon.Click += new System.EventHandler(this.pbAddPokemon_Click);
            // 
            // pbFillStorage
            // 
            this.pbFillStorage.Location = new System.Drawing.Point(23, 49);
            this.pbFillStorage.Name = "pbFillStorage";
            this.pbFillStorage.Size = new System.Drawing.Size(100, 23);
            this.pbFillStorage.TabIndex = 16;
            this.pbFillStorage.Text = "Fill Storage";
            this.pbFillStorage.UseVisualStyleBackColor = true;
            this.pbFillStorage.Click += new System.EventHandler(this.pbFillStorage_Click);
            // 
            // BattleCodes
            // 
            this.BattleCodes.Controls.Add(this.pbTrainerBattle);
            this.BattleCodes.Controls.Add(this.pbWildBattle);
            this.BattleCodes.Location = new System.Drawing.Point(12, 12);
            this.BattleCodes.Name = "BattleCodes";
            this.BattleCodes.Size = new System.Drawing.Size(251, 56);
            this.BattleCodes.TabIndex = 0;
            this.BattleCodes.TabStop = false;
            this.BattleCodes.Text = "Battle Codes";
            // 
            // TrainerCodes
            // 
            this.TrainerCodes.Controls.Add(this.SetMoney);
            this.TrainerCodes.Controls.Add(this.MoneyInput);
            this.TrainerCodes.Controls.Add(this.label3);
            this.TrainerCodes.Controls.Add(this.pbTrainerName);
            this.TrainerCodes.Controls.Add(this.label2);
            this.TrainerCodes.Controls.Add(this.label1);
            this.TrainerCodes.Controls.Add(this.TrainerType);
            this.TrainerCodes.Controls.Add(this.TrainerName);
            this.TrainerCodes.Controls.Add(this.pbUsePC);
            this.TrainerCodes.Controls.Add(this.pbPokedex);
            this.TrainerCodes.Controls.Add(this.pbPokeGear);
            this.TrainerCodes.Controls.Add(this.pbRunningShoes);
            this.TrainerCodes.Controls.Add(this.pbEmptyBag);
            this.TrainerCodes.Controls.Add(this.pbFillBag);
            this.TrainerCodes.Location = new System.Drawing.Point(12, 74);
            this.TrainerCodes.Name = "TrainerCodes";
            this.TrainerCodes.Size = new System.Drawing.Size(251, 208);
            this.TrainerCodes.TabIndex = 3;
            this.TrainerCodes.TabStop = false;
            this.TrainerCodes.Text = "Trainer Codes";
            // 
            // SetMoney
            // 
            this.SetMoney.Location = new System.Drawing.Point(191, 116);
            this.SetMoney.Name = "SetMoney";
            this.SetMoney.Size = new System.Drawing.Size(38, 23);
            this.SetMoney.TabIndex = 22;
            this.SetMoney.Text = "Set";
            this.SetMoney.UseVisualStyleBackColor = true;
            this.SetMoney.Click += new System.EventHandler(this.SetMoney_Click);
            // 
            // MoneyInput
            // 
            this.MoneyInput.Location = new System.Drawing.Point(99, 116);
            this.MoneyInput.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.MoneyInput.Name = "MoneyInput";
            this.MoneyInput.Size = new System.Drawing.Size(86, 20);
            this.MoneyInput.TabIndex = 21;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 118);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 20;
            this.label3.Text = "Set Money:";
            // 
            // pbTrainerName
            // 
            this.pbTrainerName.Location = new System.Drawing.Point(191, 55);
            this.pbTrainerName.Name = "pbTrainerName";
            this.pbTrainerName.Size = new System.Drawing.Size(38, 23);
            this.pbTrainerName.TabIndex = 19;
            this.pbTrainerName.Text = "Set";
            this.pbTrainerName.UseVisualStyleBackColor = true;
            this.pbTrainerName.Click += new System.EventHandler(this.pbTrainerName_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 13);
            this.label2.TabIndex = 18;
            this.label2.Text = "Trainer Name:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "Trainer Type: ";
            // 
            // TrainerType
            // 
            this.TrainerType.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TrainerType.FormattingEnabled = true;
            this.TrainerType.ItemHeight = 12;
            this.TrainerType.Location = new System.Drawing.Point(99, 19);
            this.TrainerType.Name = "TrainerType";
            this.TrainerType.Size = new System.Drawing.Size(130, 28);
            this.TrainerType.TabIndex = 16;
            this.TrainerType.SelectedIndexChanged += new System.EventHandler(this.TrainerType_SelectedIndexChanged);
            // 
            // TrainerName
            // 
            this.TrainerName.Location = new System.Drawing.Point(99, 55);
            this.TrainerName.MaxLength = 25;
            this.TrainerName.Name = "TrainerName";
            this.TrainerName.Size = new System.Drawing.Size(86, 20);
            this.TrainerName.TabIndex = 15;
            // 
            // pbUsePC
            // 
            this.pbUsePC.Location = new System.Drawing.Point(129, 171);
            this.pbUsePC.Name = "pbUsePC";
            this.pbUsePC.Size = new System.Drawing.Size(100, 23);
            this.pbUsePC.TabIndex = 13;
            this.pbUsePC.Text = "Use PC";
            this.pbUsePC.UseVisualStyleBackColor = true;
            this.pbUsePC.Click += new System.EventHandler(this.pbUsePC_Click);
            // 
            // pbPokedex
            // 
            this.pbPokedex.Location = new System.Drawing.Point(23, 171);
            this.pbPokedex.Name = "pbPokedex";
            this.pbPokedex.Size = new System.Drawing.Size(100, 23);
            this.pbPokedex.TabIndex = 12;
            this.pbPokedex.Text = "Pokédex";
            this.pbPokedex.UseVisualStyleBackColor = true;
            this.pbPokedex.Click += new System.EventHandler(this.pbPokedex_Click);
            // 
            // pbPokeGear
            // 
            this.pbPokeGear.Location = new System.Drawing.Point(129, 142);
            this.pbPokeGear.Name = "pbPokeGear";
            this.pbPokeGear.Size = new System.Drawing.Size(100, 23);
            this.pbPokeGear.TabIndex = 11;
            this.pbPokeGear.Text = "PokéGear";
            this.pbPokeGear.UseVisualStyleBackColor = true;
            this.pbPokeGear.Click += new System.EventHandler(this.pbPokeGear_Click);
            // 
            // pbRunningShoes
            // 
            this.pbRunningShoes.Location = new System.Drawing.Point(23, 142);
            this.pbRunningShoes.Name = "pbRunningShoes";
            this.pbRunningShoes.Size = new System.Drawing.Size(100, 23);
            this.pbRunningShoes.TabIndex = 10;
            this.pbRunningShoes.Text = "Running Shoes";
            this.pbRunningShoes.UseVisualStyleBackColor = true;
            this.pbRunningShoes.Click += new System.EventHandler(this.pbRunningShoes_Click);
            // 
            // pbEmptyBag
            // 
            this.pbEmptyBag.Location = new System.Drawing.Point(129, 81);
            this.pbEmptyBag.Name = "pbEmptyBag";
            this.pbEmptyBag.Size = new System.Drawing.Size(100, 23);
            this.pbEmptyBag.TabIndex = 7;
            this.pbEmptyBag.Text = "Empty Bag";
            this.pbEmptyBag.UseVisualStyleBackColor = true;
            this.pbEmptyBag.Click += new System.EventHandler(this.pbEmptyBag_Click);
            // 
            // pbClearStorage
            // 
            this.pbClearStorage.Location = new System.Drawing.Point(129, 49);
            this.pbClearStorage.Name = "pbClearStorage";
            this.pbClearStorage.Size = new System.Drawing.Size(100, 23);
            this.pbClearStorage.TabIndex = 17;
            this.pbClearStorage.Text = "Clear Storage";
            this.pbClearStorage.UseVisualStyleBackColor = true;
            this.pbClearStorage.Click += new System.EventHandler(this.pbClearStorage_Click);
            // 
            // PokemonCodes
            // 
            this.PokemonCodes.Controls.Add(this.pbQuickHatch);
            this.PokemonCodes.Controls.Add(this.pbHealParty);
            this.PokemonCodes.Controls.Add(this.pbClearStorage);
            this.PokemonCodes.Controls.Add(this.pbFillStorage);
            this.PokemonCodes.Controls.Add(this.pbGiveDemoParty);
            this.PokemonCodes.Controls.Add(this.pbAddPokemon);
            this.PokemonCodes.Location = new System.Drawing.Point(12, 288);
            this.PokemonCodes.Name = "PokemonCodes";
            this.PokemonCodes.Size = new System.Drawing.Size(251, 117);
            this.PokemonCodes.TabIndex = 14;
            this.PokemonCodes.TabStop = false;
            this.PokemonCodes.Text = "Pokémon Codes";
            // 
            // pbQuickHatch
            // 
            this.pbQuickHatch.Location = new System.Drawing.Point(129, 78);
            this.pbQuickHatch.Name = "pbQuickHatch";
            this.pbQuickHatch.Size = new System.Drawing.Size(100, 23);
            this.pbQuickHatch.TabIndex = 19;
            this.pbQuickHatch.Text = "Quick Hatch";
            this.pbQuickHatch.UseVisualStyleBackColor = true;
            this.pbQuickHatch.Click += new System.EventHandler(this.pbQuickHatch_Click);
            // 
            // pbHealParty
            // 
            this.pbHealParty.Location = new System.Drawing.Point(23, 78);
            this.pbHealParty.Name = "pbHealParty";
            this.pbHealParty.Size = new System.Drawing.Size(100, 23);
            this.pbHealParty.TabIndex = 18;
            this.pbHealParty.Text = "Heal Party";
            this.pbHealParty.UseVisualStyleBackColor = true;
            this.pbHealParty.Click += new System.EventHandler(this.pbHealParty_Click);
            // 
            // Input
            // 
            this.Input.AutoSize = true;
            this.Input.Checked = true;
            this.Input.CheckState = System.Windows.Forms.CheckState.Checked;
            this.Input.Location = new System.Drawing.Point(213, 411);
            this.Input.Name = "Input";
            this.Input.Size = new System.Drawing.Size(50, 17);
            this.Input.TabIndex = 15;
            this.Input.Text = "Input";
            this.Input.UseVisualStyleBackColor = true;
            this.Input.CheckedChanged += new System.EventHandler(this.Input_CheckedChanged);
            // 
            // StatusStrip
            // 
            this.StatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusLabel});
            this.StatusStrip.Location = new System.Drawing.Point(0, 432);
            this.StatusStrip.Name = "StatusStrip";
            this.StatusStrip.Size = new System.Drawing.Size(277, 22);
            this.StatusStrip.SizingGrip = false;
            this.StatusStrip.TabIndex = 16;
            this.StatusStrip.Text = "Status Strip";
            // 
            // StatusLabel
            // 
            this.StatusLabel.Font = new System.Drawing.Font("Segoe UI", 7F);
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(254, 17);
            this.StatusLabel.Text = "Remember to close this window when no longer need it!";
            // 
            // PKMNEssentials
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(277, 454);
            this.Controls.Add(this.StatusStrip);
            this.Controls.Add(this.Input);
            this.Controls.Add(this.PokemonCodes);
            this.Controls.Add(this.TrainerCodes);
            this.Controls.Add(this.BattleCodes);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PKMNEssentials";
            this.ShowIcon = false;
            this.Text = "PKMN Essentials Quick Commands";
            this.Load += new System.EventHandler(this.PKMNEssentials_Load);
            this.BattleCodes.ResumeLayout(false);
            this.TrainerCodes.ResumeLayout(false);
            this.TrainerCodes.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MoneyInput)).EndInit();
            this.PokemonCodes.ResumeLayout(false);
            this.StatusStrip.ResumeLayout(false);
            this.StatusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button pbWildBattle;
        private System.Windows.Forms.Button pbTrainerBattle;
        private System.Windows.Forms.Button pbGiveDemoParty;
        private System.Windows.Forms.Button pbFillBag;
        private System.Windows.Forms.Button pbAddPokemon;
        private System.Windows.Forms.Button pbFillStorage;
        private System.Windows.Forms.GroupBox BattleCodes;
        private System.Windows.Forms.GroupBox TrainerCodes;
        private System.Windows.Forms.Button pbUsePC;
        private System.Windows.Forms.Button pbPokedex;
        private System.Windows.Forms.Button pbPokeGear;
        private System.Windows.Forms.Button pbRunningShoes;
        private System.Windows.Forms.Button pbEmptyBag;
        private System.Windows.Forms.Button pbClearStorage;
        private System.Windows.Forms.GroupBox PokemonCodes;
        private System.Windows.Forms.Button pbQuickHatch;
        private System.Windows.Forms.Button pbHealParty;
        private System.Windows.Forms.TextBox TrainerName;
        private System.Windows.Forms.ListBox TrainerType;
        private System.Windows.Forms.Button pbTrainerName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox Input;
        private System.Windows.Forms.Button SetMoney;
        private System.Windows.Forms.NumericUpDown MoneyInput;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.StatusStrip StatusStrip;
        private System.Windows.Forms.ToolStripStatusLabel StatusLabel;
    }
}