﻿namespace RGSSUtils.Forms
{
    partial class ScriptLoader
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            Main.ScriptWindowLoaded = false;
            RGSSx.Eval("RGSSUtils.enable_input");
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.EvalButton = new System.Windows.Forms.Button();
            this.InputEnabled = new System.Windows.Forms.CheckBox();
            this.StatusStrip = new System.Windows.Forms.StatusStrip();
            this.ScriptStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.ScriptBox = new System.Windows.Forms.RichTextBox();
            this.PKMNEssentials = new System.Windows.Forms.Button();
            this.ClearScript = new System.Windows.Forms.Button();
            this.StatusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // EvalButton
            // 
            this.EvalButton.Location = new System.Drawing.Point(245, 231);
            this.EvalButton.Name = "EvalButton";
            this.EvalButton.Size = new System.Drawing.Size(103, 23);
            this.EvalButton.TabIndex = 1;
            this.EvalButton.Text = "Run Script";
            this.EvalButton.UseVisualStyleBackColor = true;
            this.EvalButton.Click += new System.EventHandler(this.EvalButton_Click);
            // 
            // InputEnabled
            // 
            this.InputEnabled.AutoSize = true;
            this.InputEnabled.Location = new System.Drawing.Point(12, 212);
            this.InputEnabled.Name = "InputEnabled";
            this.InputEnabled.Size = new System.Drawing.Size(50, 17);
            this.InputEnabled.TabIndex = 4;
            this.InputEnabled.Text = "Input";
            this.InputEnabled.UseVisualStyleBackColor = true;
            this.InputEnabled.CheckedChanged += new System.EventHandler(this.InputEnabled_CheckedChanged);
            // 
            // StatusStrip
            // 
            this.StatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ScriptStatus});
            this.StatusStrip.Location = new System.Drawing.Point(0, 257);
            this.StatusStrip.Name = "StatusStrip";
            this.StatusStrip.Size = new System.Drawing.Size(364, 22);
            this.StatusStrip.SizingGrip = false;
            this.StatusStrip.TabIndex = 5;
            this.StatusStrip.Text = "StatusStrip";
            // 
            // ScriptStatus
            // 
            this.ScriptStatus.Name = "ScriptStatus";
            this.ScriptStatus.Size = new System.Drawing.Size(39, 17);
            this.ScriptStatus.Text = "Ready";
            // 
            // ScriptBox
            // 
            this.ScriptBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ScriptBox.DetectUrls = false;
            this.ScriptBox.Location = new System.Drawing.Point(12, 12);
            this.ScriptBox.Name = "ScriptBox";
            this.ScriptBox.Size = new System.Drawing.Size(336, 190);
            this.ScriptBox.TabIndex = 1;
            this.ScriptBox.Text = "";
            this.ScriptBox.WordWrap = false;
            // 
            // PKMNEssentials
            // 
            this.PKMNEssentials.Location = new System.Drawing.Point(114, 231);
            this.PKMNEssentials.Name = "PKMNEssentials";
            this.PKMNEssentials.Size = new System.Drawing.Size(103, 23);
            this.PKMNEssentials.TabIndex = 6;
            this.PKMNEssentials.Text = "PKMN Essentials";
            this.PKMNEssentials.UseVisualStyleBackColor = true;
            this.PKMNEssentials.Visible = false;
            this.PKMNEssentials.Click += new System.EventHandler(this.PKMNEssentials_Click);
            // 
            // ClearScript
            // 
            this.ClearScript.Location = new System.Drawing.Point(12, 231);
            this.ClearScript.Name = "ClearScript";
            this.ClearScript.Size = new System.Drawing.Size(75, 23);
            this.ClearScript.TabIndex = 7;
            this.ClearScript.Text = "Clear";
            this.ClearScript.UseVisualStyleBackColor = true;
            this.ClearScript.Click += new System.EventHandler(this.ClearScript_Click);
            // 
            // ScriptLoader
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(364, 279);
            this.Controls.Add(this.ClearScript);
            this.Controls.Add(this.PKMNEssentials);
            this.Controls.Add(this.ScriptBox);
            this.Controls.Add(this.StatusStrip);
            this.Controls.Add(this.InputEnabled);
            this.Controls.Add(this.EvalButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "ScriptLoader";
            this.ShowIcon = false;
            this.Text = "Runtime Script Editor";
            this.Load += new System.EventHandler(this.ScriptLoader_Load);
            this.StatusStrip.ResumeLayout(false);
            this.StatusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button EvalButton;
        private System.Windows.Forms.CheckBox InputEnabled;
        private System.Windows.Forms.StatusStrip StatusStrip;
        private System.Windows.Forms.ToolStripStatusLabel ScriptStatus;
        private System.Windows.Forms.RichTextBox ScriptBox;
        private System.Windows.Forms.Button PKMNEssentials;
        private System.Windows.Forms.Button ClearScript;
    }
}