﻿using RGSSUtils.Forms;
using RGSSUtils.Utils;
using RGiesecke.DllExport;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;

namespace RGSSUtils
{
    public class Main
    {
        #region "Variables"
        public static IDictionary<string, string> Storage = new Dictionary<string, string>();
        public static bool ScriptWindowLoaded = false;
        public static string DLLVersion = "1.0";
        public static bool USEANTICHEAT;
        public static String GameName;
        public static String CipherKey;
        public static String HTTPMethod;
        public static String HTTPUserAgent;
        public static ScriptLoader ScriptWindow;
        public static bool Initialized;
        public static int RGSSVersion = 0;
        #endregion

        #region "Initialize Methods"
        /// <summary>
        /// Injects the Ruby code into the RGSSx
        /// </summary>
        /// <returns></returns>
        [DllExport("LoadLibrary", CallingConvention = CallingConvention.Cdecl)]
        public static bool Load()
        {
            if (RGSSx.Initialize())
            {
                ScriptWindowLoaded = false;
                for (int i = Application.OpenForms.Count - 1; i >= 0; i--)
                {
                    Application.OpenForms[i].Close();
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Initializes the Main Variables
        /// </summary>
        /// <param name="gamename"></param>
        /// <param name="cipherkey"></param>
        /// <returns></returns>
        [DllExport("Initialize", CallingConvention = CallingConvention.Cdecl)]
        public static bool Initialize(string gamename, string cipherkey, string method, string user_agent)
        {
            Initialized = true;
            GameName = gamename;
            CipherKey = cipherkey;
            HTTPMethod = method;
            HTTPUserAgent = user_agent;
            if (String.IsNullOrWhiteSpace(HTTPUserAgent))
            {
                HTTPUserAgent = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36";
            }
            if (HTTPMethod != "GET" || HTTPMethod != "POST")
            {
                HTTPMethod = "GET";
            }
            if (String.IsNullOrWhiteSpace(GameName))
            {
                GameName = "GameName";
            }
            if (String.IsNullOrWhiteSpace(CipherKey))
            {
                CipherKey = "c0b8925eefb3505cc208063d4c84a457";
            }
            return true;
        }
        #endregion

        #region "RGSS and DLL Version"
        /// <summary>
        /// Evaluates a chunk of code
        /// </summary>
        /// <param name="code"></param>
        [DllExport("Eval", CallingConvention = CallingConvention.Cdecl)]
        public static bool Eval(string code)
        {
            return RGSSx.Eval(code);
        }

        /// <summary>
        /// Returns the Library Version
        /// </summary>
        /// <returns></returns>
        [DllExport("DLLVersion", CallingConvention = CallingConvention.Cdecl)]
        public static string Version()
        {
            return DLLVersion;
        }

        /// <summary>
        /// Returns the RGSS Library Version
        /// </summary>
        /// <returns></returns>
        [DllExport("RGSSVersion", CallingConvention = CallingConvention.Cdecl)]
        public static int RGSS_Version()
        {
            return RGSSVersion;
        }
        #endregion

        #region "Encryption"
        /// <summary>
        /// Applies a Word-based Cipher to the given string
        /// </summary>
        /// <param name="data"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        [DllExport("Encrypt", CallingConvention = CallingConvention.Cdecl)]
        public static string Encrypt(string data)
        {
            if (!Initialized)
            {
                return "";
            }
            return Cipher.Encrypt(data, CipherKey);

        }

        /// <summary>
        /// Attempts to recover the value hidden in the Cipher Buffer
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        [DllExport("Decrypt", CallingConvention = CallingConvention.Cdecl)]
        public static string Decrypt(string buffer)
        {
            string data = "";
            if (!Initialized)
            {
                return "";
            }
            try
            {
                data = Cipher.Decrypt(buffer, CipherKey);
            }
            catch
            {
                data = "";
            }
            return data;

        }
        #endregion

        #region "Web Request and Storage"
        /// <summary>
        /// Sends a request to the given URL and returns the response, in case of error returns the full response
        /// </summary>
        /// <param name="host"></param>
        /// <param name="type"></param>
        /// <param name="user_agent"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        [DllExport("WebRequest", CallingConvention = CallingConvention.Cdecl)]
        public static string WebRequest(string host, string query)
        {
            if (!Initialized)
            {
                return "";
            }
            if( !(host.StartsWith("https://") || host.StartsWith("http://")))
            {
                if(host.StartsWith("//"))
                {
                    host = "http:" + host;
                }
                else
                {
                    host = "http://" + host;
                }
            }
            try
            {
                MiniWebRequest req = new MiniWebRequest(host, HTTPMethod, query, HTTPUserAgent);
                return req.GetResponse();
            }
            catch(Exception e)
            {
                return "";
            }
        }

        /// <summary>
        /// Stores a value in an internal dictionary
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        [DllExport("SetValue", CallingConvention = CallingConvention.Cdecl)]
        public static void SetValue(String key, String value)
        {
            if (Initialized) {
                Storage[key] = value;
            }
        }

        /// <summary>
        /// Gets a value from the internal dictionary and returns it, if not exists returns an empty string
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        [DllExport("GetValue", CallingConvention = CallingConvention.Cdecl)]
        public static String GetValue(String key)
        {
            string value;
            if (!Initialized)
            {
                return "";
            }
            try
            {
                value = Storage[key];
            }
            catch
            {
                value = "";
            }
            return value;

        }
        #endregion

        #region "File Download"
        /// <summary>
        /// Downloads a file
        /// </summary>
        /// <param name="url"></param>
        /// <param name="filename"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [DllExport("FileDownload", CallingConvention = CallingConvention.Cdecl)]
        public static bool FileDownload(string url, string filename, int id)
        {
            if (!Initialized)
            {
                return false;
            }
            else
            {
                FileDownload download = new FileDownload(url, filename, id);
                download.Start();
                return true;
            }
        }
        #endregion

        #region "Custom File Packaging"
        /// <summary>
        /// Dumps all the vars data to a file
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        [DllExport("DumpData", CallingConvention = CallingConvention.Cdecl)]
        public static void DumpData(string ID, string filename)
        {
            if (Initialized)
            {
                SymmetricAlgorithm Enc = SymmetricAlgorithm.Create();
                FileHandler.EncryptToFile(filename, Enc, Storage);
                WindowsRegistry.Store(String.Format("{0} Key", ID), GetString(Enc.Key, true));
                WindowsRegistry.Store(String.Format("{0} IV", ID), GetString(Enc.IV, true));
            }
        }

        /// <summary>
        /// Dumps all the vars data to a file
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="cipher"></param>
        /// <returns></returns>
        [DllExport("LoadData", CallingConvention = CallingConvention.Cdecl)]
        public static bool LoadData(string ID, string filename, string cipher)
        {
            if (!Initialized)
            {
                return false;
            }
            try
            {
                SymmetricAlgorithm Enc = SymmetricAlgorithm.Create();
                Enc.Key = GetBytes(Cipher.Decrypt(WindowsRegistry.Get(String.Format("{0} Key", ID)), CipherKey));
                Enc.IV = GetBytes(Cipher.Decrypt(WindowsRegistry.Get(String.Format("{0} IV", ID)), CipherKey));
                Storage = FileHandler.DecryptFromFile(filename, Enc);
                return true;
            }
            catch
            {
                return false;
            }

        }
        #endregion

        #region "Checksum Generation"
        /// <summary>
        /// Returns the Cipher::MD5 checksum of the given string
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [DllExport("MD5", CallingConvention = CallingConvention.Cdecl)]
        public static string CalculateMD5Hash(string input)
        {
            if (!Initialized)
            {
                return "";
            }
            StringBuilder hash = new StringBuilder();
            MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider();
            byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(input));

            for (int i = 0; i < bytes.Length; i++)
            {
                hash.Append(bytes[i].ToString("x2"));
            }
            return hash.ToString();
        }

        /// <summary>
        /// Returns the Cipher::SHA1 checksum of the given string
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [DllExport("SHA1", CallingConvention = CallingConvention.Cdecl)]
        public static string SHA1(string input)
        {
            if (!Initialized)
            {
                return "";
            }
            using (SHA1Managed sha1 = new SHA1Managed())
            {
                var hash = sha1.ComputeHash(Encoding.UTF8.GetBytes(input));
                var sb = new StringBuilder(hash.Length * 2);
                foreach (byte b in hash)
                {
                    sb.Append(b.ToString("x2"));
                }
                return sb.ToString();
            }
        }
        #endregion

        #region "PC Info"
        /// <summary>
        /// Gets the in-use Network Device's MAC Address
        /// </summary>
        /// <returns></returns>
        [DllExport("GetMACAddress", CallingConvention = CallingConvention.Cdecl)]
        public static string getMACAddress()
        {
            if (!Initialized)
            {
                return "";
            }
            return GetComputerInfo.getMACAddress();
        }
        #endregion

        #region "Support Methods (Private)"
        /// <summary>
        /// Converts a String into a Bytes Array
        /// </summary>
        /// <param name="string"></param>
        /// <returns></returns>
        public static byte[] GetBytes(string text)
        {
            byte[] bytes = new byte[text.Length * sizeof(char)];
            System.Buffer.BlockCopy(text.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        /// <summary>
        /// Converts a Byte Array into a String
        /// </summary>
        /// <param name="bytes"></param>
        /// <param name="wCipher">Encrypts the string with an internal key</param>
        /// <returns></returns>
        public static string GetString(byte[] bytes, bool wCipher = false)
        {
            char[] chars = new char[bytes.Length / sizeof(char)];
            System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            string str = new string(chars);
            if (wCipher)
            {
                str = Cipher.Encrypt(str, CipherKey);
            }
            return str;

        }
        #endregion

        #region "Script Loader"
        [DllExport("ShowScriptWindow", CallingConvention = CallingConvention.Cdecl)]
        static void ShowScriptWindow()
        {
            if (ScriptWindowLoaded)
            {
                return;
            }
            ScriptWindow = new ScriptLoader();
            ScriptWindow.Show();
            ScriptWindowLoaded = true;
            RGSSx.Eval("RGSSUtils.disable_input");
        }
        #endregion
    }
}
